# Photos-like App

Design an iOS app that is similar with **Photos** .

## Requirements

* Select all photos (excluded videos) in the **Camera Roll** album in Photos. (Need check access permission)

* Show all photos as below

    ![Grid view](grid.jpeg)

* Show selected photo after clicking any photos in the grid view. (Show filename and date at the title)

    ![Grid view](single.jpeg)

* You need implement some functions in the photo view as below : 

    * Zoom in/out

    * Slide to previous/next photo

    * Add a **back** button that can turn back to the grid view

## Optional

* You can add more features in your **Photos-like** app, like : 

    * Support **landscape**

    * Show photo's **EXIF** or **GPS information**

    * Any feature in iOS **Photos** app

## Note

* You can't import or use any third-party **framework / library**.